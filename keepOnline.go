package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/kardianos/service"
)

const (
	//VERSION VERSION
	VERSION = "0.0.2"
)

type program struct {
	log service.Logger
	cfg *service.Config
}

//Config file struct
type Config struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Version  string `json:"version"`
	URL      string `json:"url"`
	TTLhours int    `json:"TTLhours"`
}

var configs Config

func (p *program) Start(s service.Service) error {
	go p.run()
	return nil
}

func (p *program) run() {
	SendRequest()
	TimerAlert()
	wg.Done()
}

func (p *program) Stop(s service.Service) error {
	s.Stop()
	return nil
}

var wg sync.WaitGroup

func init() {
	log.SetFlags(log.Lshortfile | log.LstdFlags | log.Lmicroseconds)
	configfile, e := ioutil.ReadFile("config.json")
	if e != nil {
		log.Printf("configfile File read error: %v\n", e)
	}
	json.Unmarshal(configfile, &configs)
	log.Printf("config.json load finish.\n")
}

func main() {
	f, err := os.OpenFile("KO.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer f.Close()
	wrt := io.MultiWriter(os.Stdout, f)
	log.SetOutput(wrt)

	log.Printf("Server v%s pid=%d started with processes: %d", VERSION, os.Getpid(), runtime.GOMAXPROCS(runtime.NumCPU()))
	log.Println("Keep Online Start!!!")

	wg.Add(1)
	svcConfig := &service.Config{
		Name:        "Keep Online",
		DisplayName: "9skeepOnline",
		Description: "This keep internet connection for 9splay",
	}
	prg := &program{}
	s, err := service.New(prg, svcConfig)
	if err != nil {
		log.Println(err.Error())
	}
	if len(os.Args) > 1 {
		if os.Args[1] == "install" {
			x := s.Install()
			if x != nil {
				fmt.Println("error:", x.Error())
				return
			}
			s.Run()
			log.Println("[[服務安裝成功]]")
			return
		} else if os.Args[1] == "uninstall" {
			s.Stop()
			x := s.Uninstall()
			if x != nil {
				fmt.Println("error:", x.Error())
				return
			}

			log.Println("[[ 服務移除成功 ]]")
			os.Exit(0)
			return
		}
	}
	err = s.Run()
	if err != nil {
		log.Println(err.Error())
	}
	wg.Wait()
}

//TimerAlert Timer
func TimerAlert() {
	ticker := time.NewTicker(time.Duration(configs.TTLhours) * 60 * 60 * time.Second)
	//ticker := time.NewTicker(1 * 60 * 60 * time.Second)
	for range ticker.C {
		defer ticker.Stop()
		timestamp := time.Now().Unix()
		tm := time.Unix(timestamp, 0)
		log.Println(tm.Format("2006-01-02 15:04:05"))
		go SendRequest()
	}
}

//SendRequest to Auth
func SendRequest() {
	var r http.Request
	timestamp := time.Now().Unix()
	s := strconv.Itoa(int(timestamp))
	r.ParseForm()
	r.Form.Add("username", configs.Username)
	r.Form.Add("password", configs.Password)
	r.Form.Add("mode", "191")
	r.Form.Add("producttype", "0")
	r.Form.Add("a", s)
	bodystr := strings.TrimSpace(r.Form.Encode())
	request, err := http.NewRequest("POST", configs.URL, strings.NewReader(bodystr))
	if err != nil {
	}
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	request.Header.Set("Connection", "Keep-Alive")

	var resp *http.Response
	resp, err = http.DefaultClient.Do(request)
	if err != nil {
		log.Println("Request Error:", err)
	}
	log.Println(" === Request Finish === [Response : ", resp.Status, "]")
	defer resp.Body.Close()
}
